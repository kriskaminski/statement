import { NodeBlock } from "tripetto-runner-foundation";

export interface IStatement {
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
}

export abstract class Statement extends NodeBlock<IStatement> {}
